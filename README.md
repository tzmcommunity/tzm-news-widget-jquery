# tzm-news-widget-jquery
A jQuery based News Widget which makes it easy to embed the latest TZM news entries on a website

Checkout the example.html file for an example usage

This is designed to go on your website, be it a Wordpress site or raw HTML page.

# Requirements
The widget requires jQuery. If not included it attempts to add jQuery 3 itself, but this doesn't always work properly.

You'll also need a div or placeholder which the widget can use. Check the Usage section.

# Usage

It's expected you'll have a div you can output the contents to. 
The default is:

    <div id="tzm-news-widget"></div>

However you can define a different location, like using a class instead of an ID either using setConfig
or when calling render();


Example usage:

    <div class="tzm-news-widget"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="tzm-news-widget.js"></script>
    <script type="text/javascript">
        jQuery(function () {
            // On Dom Ready
            _tzm_news_widget.setConfig({
                maxEntries: 5,
                headerType: 'full',
                dateType: 'medium'
            });
            _tzm_news_widget.render('.tzm-news-widget');
        });
    </script>

There's a few options you can configure with the setConfig including:

1. **maxEntries** - Defaults to 10. Sets how many TZM News entries to show at a time. Note that in the post pagination there's a `[View All]` button which sets this to the number of entries.
Currently ALL the JSON is downloaded at once. So this field only changes how many are shown
1. **showPagination** - Defaults to true. Set this to false to turn off the pre/post messages about how many entries are being shown.
1. **showPrePagination** - Defaults to true. Set this to false to turn off the pre (before posts) messages about how many entries are being shown.
1. **showPostPagination** - Defaults to true. Set this to false to turn off the post (after posts) messages about how many entries are being shown, and also the button which lets people view ALL the news entries.
1. **headerType** - Defaults to 'full'. Accepts: 'full', 'medium', 'short', 'none'. These are for the format of the post headers. Try these out to see which style works best
1. **dateType** - Defaults to 'medium', // Accepts: 'full', 'medium', 'short'. This is for the date output in the headers. Try these out to see which style works best
1. **newsNotAvailableMessage** - Defaults to a formatted Error about being `Unable to get any TZM News entries.`. Only shown if there was an issue getting the messages.json file.
1. **newsLoadingMessage** - Defaults to a formatted info message about `Loading theTZM News Entries please wait.`. It's usually shown briefly when the Ajax GET request is being done.
1. **location** - Defaults to `#tzm-news-widget`, // location of a html element (preferably an empty div) which is filled with the tzm-news-widget
# Styles
You can include the CSS files. There's a couple of different styles

Everything is output with `tzm-news` as the CSS class prefix. e.g `.tzm-news-widget` or `.tzm-news-post`

There's 3 main CSS styles

### Full
This includes

    <link rel="stylesheet" type="text/css" href="tzm-news-widget-full.css">

### Clean
This version has a border around the edges but not between each post    
    
    <link rel="stylesheet" type="text/css" href="tzm-news-widget-clean.css">
    
### Minimal
This is very basic, just enough to not look REALLY ugly. But doesn't look very pretty either.
It's expected you'll add your own local styling to this, or your site already has it's own styling.

    <link rel="stylesheet" type="text/css" href="tzm-news-widget-minimal.css">
    
    